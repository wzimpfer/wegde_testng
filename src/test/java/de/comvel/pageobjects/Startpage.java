package de.comvel.pageobjects;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


/**
 *
 * @author waldemarzimpfer
 */
public class Startpage {

    private static WebDriver driver;  
    private String tcprfx = "WEG-1";

    public Startpage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "cx_Quicksearch_CvLastMinute_Autocomplete")
    WebElement inptReiseziel;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_EarliestDeparture")
    WebElement inptDateFrom;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_LatestReturn")
    WebElement inptDateTo;

    @FindBy(name = "duration")
    WebElement duration;

    @FindBy(name = "travellers")
    WebElement travellers;

    public boolean isStartpageLoaded() {
        driver.get("http://weg.de");

        if (driver.getTitle().equals("weg.de - Urlaub günstig buchen beim mehrfachen Testsieger")) {
    //        tlc.reportResult(tcprfx, 1, "", getPlattformsbyName("chrome"), ExecutionStatus.PASSED);
            return true;            
        } else {
      //      tlc.reportResult(tcprfx, 1, "", getPlattformsbyName("chrome"), ExecutionStatus.FAILED);
            return false;
        }
    }

    public boolean isInputDestinationEditable(String reiseziel) {
        try {
            inptReiseziel.sendKeys(reiseziel);
            inptReiseziel.sendKeys(Keys.ENTER);
            //Thread.sleep(2000);            
            if (inptReiseziel.getAttribute("value").equals(reiseziel)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Reiseziel' konnte nicht lokalisiert werden \n " + e.getMessage());
            return false;
        }
    }

    public boolean isInputDateFromEditable(String fromDate) {
        try {
            /*            
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");            
            Date date = new Date(fromDate);
            
            System.out.println(df.format(date));*/
            inptDateFrom.click();

            WebElement calendar = driver.findElement(By.className("cx_Calendar_Months"));
            List<WebElement> months = calendar.findElements(By.tagName("table"));

            for (int i = 0; i < months.size(); i++) {
                if (months.get(i).findElement(By.tagName("caption")).getText().contains("September 2016")) {
                    System.out.println(months.get(i).findElement(By.tagName("caption")).getText());
                    List<WebElement> days = months.get(i).findElements(By.tagName("td"));
                    for (WebElement day : days) {
                        if (day.getText().equals("30")) {
                            day.click();
                            break;
                        }
                    }

                }
            }
            if (inptDateFrom.getAttribute("value").contains(fromDate)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Frühester Hinflug' konnte nicht lokalisiert werden \n " + e.getMessage());
            return false;
        }
    }

    public boolean isInputDateToEditable(String toDate) {
        try {
            inptDateTo.click();

            WebElement calendar = driver.findElement(By.className("cx_Calendar_Months"));
            List<WebElement> months = calendar.findElements(By.tagName("table"));

            for (int i = 0; i < months.size(); i++) {
                if (months.get(i).findElement(By.tagName("caption")).getText().contains("Oktober 2016")) {
                    List<WebElement> days = months.get(i).findElements(By.tagName("td"));
                    for (WebElement day : days) {
                        if (day.getText().equals("20")) {
                            day.click();
                            break;
                        }
                    }

                }
            }
            if (inptDateTo.getAttribute("value").contains(toDate)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Spätester Rückflug' konnte nicht lokalisiert werden \n " + e.getMessage());
            return false;
        }
    }

    public boolean isDurationChooseble(String dur) {
        try {
            Select durSelect = new Select(duration);
            durSelect.selectByVisibleText(dur);
            if (durSelect.getFirstSelectedOption().getText().equals(dur)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Select 'Reisedauer' konnte nicht lokalisiert werden");
            return false;
        }
    }

 /*   String getPlattformsbyName(String platform) {
        String platformName = null;
        for (int i = 0; i < plattforms.length; i++) {
            if (plattforms[i].getName().toLowerCase().contains(platform)) {
                platformName = plattforms[i].getName();
            }
        }
        return platformName;
    }*/

    public boolean isTrevellersChooseble(String countTrvl) {
        return true;
    }

}
