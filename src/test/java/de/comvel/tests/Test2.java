/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.comvel.tests;

import de.comvel.pageobjects.Startpage;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author waldemarzimpfer
 */
public class Test2 {
    
    public Test2() {
    }

    static WebDriver driver;
    static Startpage startpage;

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.setProperty("webdriver.chrome.driver", "Chromedriver/chromedriver");
        DesiredCapabilities caps = new DesiredCapabilities(DesiredCapabilities.chrome());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        driver = new RemoteWebDriver(new URL("http://192.168.2.17:4444/wd/hub"),caps);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);    
        startpage = new Startpage(driver);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        driver.quit();
    }
    
    @Test
    public void b_WEG_18_Last_Minute_2_Personen() throws InterruptedException {
        
        assertTrue(startpage.isStartpageLoaded());
        
        assertTrue(startpage.isInputDestinationEditable("Spanien"),"Input 'Reiseziel' konnte nicht editiert werden");
        
        assertTrue(startpage.isInputDateFromEditable("30.09.2016"), "Input 'Frühester Hinflug' konnte nicht editiert werden");
        
        assertTrue(startpage.isInputDateToEditable("20.10.2016"), "Input 'Spätester Rückflug' konnte nicht editiert werden");
        
        assertTrue(startpage.isDurationChooseble("2 Wochen"),"Select 'Reisedauer' konnte nicht ausgewählt werden");
    }
        

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
